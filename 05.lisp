(defparameter input-path #P"./inputs/05-input")

(require "asdf")
(asdf:load-system "uiop")

(defun decode (code zero-c one-c)
  (let ((binary-code))
    (loop for c across code do
	 (cond
	   ((eq c zero-c) (push #\0 binary-code))
	   ((eq c one-c) (push #\1 binary-code))))
    (setq binary-code
     	  (concatenate 'string
     		       (nreverse binary-code)))
    (parse-integer binary-code
		   :radix 2)))

(decode "RLL" #\L #\R)
(decode "FBFBBFF" #\F #\B)

(defun parse-boarding-pass (str)
  (let ((row-code (subseq str 0 8))
	(col-code (subseq str 7)))
    (list (decode row-code #\F #\B)
	  (decode col-code #\L #\R))))

(defun parse-input (inp)
  (mapcar #'parse-boarding-pass inp))

(defparameter inp (parse-input (uiop:read-file-lines input-path)))

(defun boarding-id (boarding-pass)
  (+ (* 8 (first boarding-pass))
     (second boarding-pass)))

(loop for bp in inp
   maximize (boarding-id bp))

(let ((ids (sort
	    (mapcar #'boarding-id inp)
	    #'<)))

  (loop for a in ids
     for b in (cdr ids)
     when (= 2 (- b a))
     return (+ 1 a)))
