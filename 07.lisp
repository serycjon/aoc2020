(defparameter input-path #P"./inputs/07-input")

(require "asdf")
(asdf:load-system "uiop")
(asdf:load-system "cl-ppcre")

(defun parse-color (color-str)
  (read-from-string
   (cl-ppcre:regex-replace-all " " color-str "-")))

(defun parse-line (line)
  "returns alist entry (parent-color . children), where children is alist of (child-count . child-color)"
  (let* ((cleaned-line (cl-ppcre:regex-replace-all "(( bags?)|\\.|,)" line ""))
	 (src-dst-strings (cl-ppcre:split " contain " cleaned-line))
	 (src-string (first src-dst-strings))
	 (src (parse-color src-string))
	 (dst-string (second src-dst-strings))
	 (dst-split (cl-ppcre:all-matches-as-strings "(\\d+ \\w+ \\w+)" dst-string)))
    (cons
     src
     (loop :for dst-bags :in dst-split
	   :collecting
	   (let ((bag-count (parse-integer (subseq dst-bags 0 1)))
		 (bag-color (parse-color (subseq dst-bags 2))))
	     (cons bag-count bag-color))))))

(defun get-parents (color tree)
  (loop :for parent :in tree
	:when (rassoc color (cdr parent))
	  :collect (car parent)))

(defun get-all-parents (color tree &optional all-parents)
  (let ((direct-parents (get-parents color tree)))
    (if (not direct-parents)
	all-parents
	(reduce #'union
		(loop :for parent :in direct-parents
		      :collecting (get-all-parents parent tree (adjoin parent all-parents)))))))

;; (get-parents 'muted-brown (list (parse-line "dark olive bags contain 2 muted brown bags, 1 mirrored tomato bag, 4 bright black bags.")))
;; (cdr (parse-line "dark olive bags contain 2 muted brown bags, 1 mirrored tomato bag, 4 bright black bags."))
;; (parse-line "vibrant red bags contain no other bags.")

(defun get-children (color tree)
  (cdr (assoc color tree)))

(defun count-all-children (color tree)
  (let ((children (get-children color tree)))
    (if (not children)
	0
	(loop :for child :in children
	      :summing (+ (car child)
			  (* (car child) (count-all-children (cdr child) tree)))))))

;; (get-children 'shiny-gold inp)
;; (count-all-children 'vibrant-red inp)
;; (count-all-children 'dark-olive inp)
;; (count-all-children 'shiny-gold inp)

(defun parse-input (lines)
  (loop :for line :in lines :collecting (parse-line line)))

(defparameter inp (parse-input (uiop:read-file-lines input-path)))

(let ((parents (get-parents 'shiny-gold inp)))
  parents)

(format t "First task: ~A~%" (length (get-all-parents 'shiny-gold inp)))
(format t "Second task: ~A~%" (count-all-children 'shiny-gold inp))
