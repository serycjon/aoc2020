(defparameter input-path #P"./inputs/04-input")

(require "asdf")
(asdf:load-system "uiop")

(defun parse-input (lines)
  (let ((result)
	(current))
    (dolist (line lines)
      (if (= 0 (length line))
	  (progn (push current result)
		 (setq current nil))
	  (let ((entries (uiop:split-string line :separator " ")))
	    (dolist (entry entries)
	      (destructuring-bind (k v) (uiop:split-string entry :separator ":")
		(setq current
		      (acons (read-from-string k) v current)))))))
    (push current result)
    result))

(defparameter inp (parse-input (uiop:read-file-lines input-path)))

(defun full-entry-p (entry)
  (let ((req-fields '(byr iyr eyr hgt hcl ecl pid)))
    (every (lambda (x)
	     (assoc x entry))
	   req-fields)))

(defun between (x lb ub)
  (when (and x (>= x lb) (<= x ub))
    x))

(defun parse-byr (x)
  (let ((int-x (parse-integer x :junk-allowed t)))
    (between int-x 1920 2002)))

(defun parse-iyr (x)
  (let ((int-x (parse-integer x :junk-allowed t)))
    (between int-x 2010 2020)))

(defun parse-eyr (x)
  (let ((int-x (parse-integer x :junk-allowed t)))
    (between int-x 2020 2030)))

(defun parse-hgt (x)
  (let ((unit (read-from-string (subseq x (- (length x) 2))))
	(int-x (parse-integer x :junk-allowed t)))
    (values
     (cond
       ((eq unit 'cm) (between int-x 150 193))
       ((eq unit 'in) (between int-x 59 76))
       (t nil))
     unit)))

(defun char-hexa-p (c)
  (and (alphanumericp c)
       (char<= c #\f)))

(defun parse-hcl (x)
  (and
   (eq #\# (char x 0))
   (every #'char-hexa-p (subseq x 1))
   x))

(defun parse-ecl (x)
  (car (member x '(amb blu brn gry grn hzl oth)
	       :test #'string-equal)))

(defun parse-pid (x)
  (and (= 9 (length x))
       (every #'digit-char-p x)
       x))

(defun valid-entry-p (entry)
  (and (full-entry-p entry)
       (parse-byr (cdr (assoc 'byr entry)))
       (parse-iyr (cdr (assoc 'iyr entry)))
       (parse-eyr (cdr (assoc 'eyr entry)))
       (parse-hgt (cdr (assoc 'hgt entry)))
       (parse-hcl (cdr (assoc 'hcl entry)))
       (parse-ecl (cdr (assoc 'ecl entry)))
       (parse-pid (cdr (assoc 'pid entry)))))

(format t "First task: ~A~%" 
	(loop for entry in inp
	      count (full-entry-p entry)))

(format t "Second task: ~A~%"
	(loop for entry in inp
	      count (valid-entry-p entry)))


