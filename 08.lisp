;; (defparameter input-path #P"./inputs/08-test-broken")
(defparameter input-path #P"./inputs/08-input")

(require "asdf")
(asdf:load-system "uiop")

(defun parse-input (lines)
  (let ((inp-list (loop :for line :in lines :collecting
		       (let ((split (uiop:split-string line :separator " ")))
			 (cons (read-from-string (first split))
			       (parse-integer (second split)))))))
    (make-array (length inp-list)
		:initial-contents inp-list)))

(defparameter inp (parse-input (uiop:read-file-lines input-path)))

(defun run-console (mem)
  (let* ((pc 0)
	 (accumulator 0)
	 (n-instr (length mem))
	 (seen (make-array n-instr
			   :initial-element nil)))
    (loop :while
	  (and (< pc n-instr) (not (aref seen pc))) :do
	    (setf (aref seen pc) t)
	    (let ((inst (aref mem pc)))
	      (case (car inst)
		(nop (incf pc))
		(acc (incf accumulator (cdr inst))
		 (incf pc))
		(jmp (incf pc (cdr inst))))))
    (values accumulator (= pc n-instr))))

(format t "First part: ~A" (run-console inp))

(defun copy-mem (mem)
  (let ((new-mem (make-array (length mem))))
    (loop :for i :below (length mem) :do
      (setf (aref new-mem i)
	    (cons (car (aref mem i))
		  (cdr (aref mem i)))))
    new-mem)
  ;; (read-from-string (format nil "~A" mem))  ; reader macro copy-mem :D. it works!
  )

(defun fix-instruction (mem i)
  (let ((fixed-mem (copy-mem mem)))
    (case (car (aref fixed-mem i))
      (nop (setf (car (aref fixed-mem i)) 'jmp))
      (jmp (setf (car (aref fixed-mem i)) 'nop)))
    fixed-mem))

(defun fix-console (mem)
  (let ((n-instr (length mem))
	(result))
    (loop :for i :below n-instr :do
      (multiple-value-bind (accumulator finished) (run-console (fix-instruction mem i))
	(when finished
	  (setq result accumulator))))
    result))

(format t "Second part: ~A" (fix-console inp))

