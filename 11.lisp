;; (defparameter input-path #P"./inputs/11-example")
(defparameter input-path #P"./inputs/11-input")
(defparameter inp (load-input input-path))

(require "asdf")
(asdf:load-system "uiop")

(defun load-input (path)
  (let ((list-grid (mapcar (lambda (line)
			     (coerce line 'list))
			   (uiop:read-file-lines path))))
    (make-array (list (length list-grid)
		      (length (first list-grid)))
		:initial-contents list-grid)))

(defun copy-array (array &key (element-type (array-element-type array))
			   (fill-pointer (and (array-has-fill-pointer-p array)
					      (fill-pointer array)))
			   (adjustable (adjustable-array-p array)))
  "Returns an undisplaced copy of ARRAY, with same fill-pointer and
adjustability (if any) as the original, unless overridden by the keyword
arguments.
from: https://gitlab.common-lisp.net/alexandria/alexandria/-/blob/master/alexandria-1/arrays.lisp"
  (let* ((dimensions (array-dimensions array))
	 (new-array (make-array dimensions
				:element-type element-type
				:adjustable adjustable
				:fill-pointer fill-pointer)))
    (dotimes (i (array-total-size array))
      (setf (row-major-aref new-array i)
	    (row-major-aref array i)))
    new-array))

(defun copy-grid (grid)
  (copy-array grid))

(defun in-bounds (row col array)
  (destructuring-bind (H W) (array-dimensions array)
   (and (>= row 0) (>= col 0)
	(< row H) (< col W))))

(defun count-neighbors (grid row col)
  (let ((deltas '((0 1) (1 1) (1 0) (1 -1)
		  (0 -1) (-1 -1) (-1 0) (-1 1))))
    (loop :for delta :in deltas
	  :counting (let ((n-row (+ row (first delta)))
			  (n-col (+ col (second delta))))
		      (and (in-bounds n-row n-col grid)
			   (eq #\# (aref grid n-row n-col)))))))

(defun part1-update (current neighbor-count)
  (cond ((and (char= current #\L)
			      (= neighbor-count 0))
			 #\#)
			((and (char= current #\#)
			      (>= neighbor-count 4))
			 #\L)
			(t current)))

(defun part2-update (current neighbor-count)
  (cond ((and (char= current #\L)
			      (= neighbor-count 0))
			 #\#)
			((and (char= current #\#)
			      (>= neighbor-count 5))
			 #\L)
			(t current)))

(defun iter-grid (grid &optional (count-fn #'count-neighbors)
			 (update-fn #'part1-update))
  (let ((new-grid (copy-grid grid))
	(counts (make-array (array-dimensions grid) :initial-element 0)))
    (loop :for row :below (first (array-dimensions grid)) :do
      (loop
	:for col :below (second (array-dimensions grid)) :do
	  (let ((neighbor-count (funcall count-fn grid row col))
		(current (aref grid row col)))
	    (setf (aref counts row col) neighbor-count)
	    (setf (aref new-grid row col)
		  (funcall update-fn current neighbor-count)))))
    (values new-grid counts)))

(defun grid-equal (a b)
  (loop :for row :below (first (array-dimensions a)) :do
    (loop :for col :below (second (array-dimensions a)) :do
      (when (not (char= (aref a row col) (aref b row col)))
	(return-from grid-equal nil))))
  t)

(defun iter-until-equilibrium (inp iter-fun)
 (loop :for last = inp :then current
       :for current = (funcall iter-fun last)
       :when (grid-equal last current) :do
	 (return current)))


(defun count-grid (grid &optional (c #\#))
  (loop :for i :below (array-total-size grid)
	:counting (eq (row-major-aref grid i) c)))

(format t "Part 1: ~A~%" (count-grid (iter-until-equilibrium inp #'iter-grid)))

(defun can-see-full (grid row col d-row d-col)
  (loop :for cur-row = (+ row d-row) :then (+ cur-row d-row)
	:for cur-col = (+ col d-col) :then (+ cur-col d-col) :do
	  (when (not (in-bounds cur-row cur-col grid))
	    (return nil))
	  (when (eq (aref grid cur-row cur-col) #\L)
	    (return nil))
	  (when (eq (aref grid cur-row cur-col) #\#)
	    (return t))))

(defun count-visible-neighbors (grid row col)
  (let ((deltas '((0 1) (1 1) (1 0) (1 -1)
		  (0 -1) (-1 -1) (-1 0) (-1 1))))
    (loop :for delta :in deltas
	  :counting (can-see-full grid row col (first delta) (second delta)))))

(defun part2-iter-grid (grid)
  (iter-grid grid #'count-visible-neighbors #'part2-update))

(format t "Part 2: ~A~%"
	(count-grid (iter-until-equilibrium inp #'part2-iter-grid)))
