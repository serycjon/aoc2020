(require "asdf")
(asdf:load-system "uiop")

(defun parse-tile (line)
  (cond
    ((string= line "") nil)
    ((string= line "e" :end1 1) (cons 'e (parse-tile (subseq line 1))))
    ((string= line "w" :end1 1) (cons 'w (parse-tile (subseq line 1))))
    ((string= line "ne" :end1 2) (cons 'ne (parse-tile (subseq line 2))))
    ((string= line "nw" :end1 2) (cons 'nw (parse-tile (subseq line 2))))
    ((string= line "se" :end1 2) (cons 'se (parse-tile (subseq line 2))))
    ((string= line "sw" :end1 2) (cons 'sw (parse-tile (subseq line 2))))))

;; cube coordinates (https://www.redblobgames.com/grids/hexagons/)
(defparameter deltas '((ne . (1 0 -1))
		       (nw . (0 1 -1))
		       (se . (0 -1 1))
		       (sw . (-1 0 1))
		       (e . (1 -1 0))
		       (w . (-1 1 0))))

(defun parse-input (path)
  (loop :for line :in (uiop:read-file-lines path)
	:collect (parse-tile line)))

(defparameter inp (parse-input "inputs/24-example"))
(defparameter inp (parse-input "inputs/24-input"))

(defun tile-to-coords (tile)
  (let ((coords '(0 0 0)))
    (loop :for direction :in tile :do
      (setq coords
	    (mapcar #'+
		    coords
		    (cdr (assoc direction deltas)))))
    coords))

(defun part1 (inp)
  (let ((tile-map (make-hash-table :test 'equal)))
    (loop :for tile :in inp :do
      (setf (gethash (tile-to-coords tile) tile-map)
	    (if (gethash (tile-to-coords tile) tile-map)
		nil
		t)))

    (loop :for v :being :the hash-value :of tile-map
	  :count v)))

(format t "Part 1: ~a~%" (part1 inp))

(defun tile-neighbors (coords)
  (loop :for (name . delta) :in deltas
	:collect (mapcar #'+ coords delta)))

(defun interesting-tiles (tile-map)
  (let ((interesting (make-hash-table :test 'equal)))
    (loop :for coords :being :the hash-key :in tile-map :using (hash-value v) :when v :do
      (setf (gethash coords interesting) t)
      (loop :for neigh :in (tile-neighbors coords) :do
	(setf (gethash neigh interesting) t)))

    (loop :for coords :being :the hash-key :in interesting
	  :collect coords)))

(defun flipped-color (coords tile-map)
  (let ((n-black-around (loop :for neigh :in (tile-neighbors coords)
			      :count (gethash neigh tile-map)))
	(current-black (gethash coords tile-map)))
    (cond
      ((and current-black
	    (or (= n-black-around 0)
		(> n-black-around 2)))
       'white)
      ((and (not current-black)
	    (= n-black-around 2))
       'black))
    ))

(defun flip-tiles (orig-tile-map)
  (let* ((interesting (interesting-tiles orig-tile-map))
	 (new-colors (mapcar (lambda (tile)
			       (flipped-color tile orig-tile-map))
			     interesting)))
    ;; (values interesting new-colors)
    (mapcar (lambda (coord new-color)
	      (cond
		((eq 'black new-color) (setf (gethash coord orig-tile-map) t))
		((eq 'white new-color) (setf (gethash coord orig-tile-map) nil))))
	    interesting new-colors)
    orig-tile-map))

(defun count-black (tile-map)
  (loop :for v :being :the hash-value :of tile-map
	:count v))

(defun part2 (inp)
  (let ((tile-map (make-hash-table :test 'equal)))
    (loop :for tile :in inp :do
      (setf (gethash (tile-to-coords tile) tile-map)
	    (if (gethash (tile-to-coords tile) tile-map)
		nil
		t)))

    (loop :for i :below 100 :do
      ;; (format t "Day ~a: ~a~%" i (count-black tile-map))
      (flip-tiles tile-map))

    (count-black tile-map)))

(format t "Part 2: ~a~%" (part2 inp))
