(require "asdf")
(asdf:load-system "uiop")
(ql:quickload "cl-ppcre")

(defun parse-input (path)
  (let ((player1)
	(player2)
	(p1-finished))
    (loop :for line :in (uiop:read-file-lines path) :do
      (when (not (cl-ppcre:scan "Player" line))
	(if (= 0 (length line))
	    (setq p1-finished t)
	    (if p1-finished
		(push (parse-integer line) player2)
		(push (parse-integer line) player1)))))
    (list (reverse player1) (reverse player2))))

(defparameter inp (parse-input "inputs/22-example"))
(defparameter inp (parse-input "inputs/22-loop"))
(defparameter inp (parse-input "inputs/22-input"))

(defun play-round (xs ys)
  (let ((x (car xs))
	(rest-x (cdr xs))
	(y (car ys))
	(rest-y (cdr ys)))
    (if (> x y)
	(list (append rest-x (list (max x y) (min x y)))
	      rest-y)
	(list rest-x
	      (append rest-y (list (max x y) (min x y)))))))

(defun score (deck)
  (loop :for x :in (reverse deck)
	:for i :from 1 :to (length deck)
	:sum (* i x)))

(format t "Part 1: ~A~%"
 (let ((p1-deck (copy-list (first inp)))
       (p2-deck (copy-list (second inp))))
   (loop :while (> (* (length p1-deck)
		      (length p2-deck))
		   0)
	 :do (let ((new-decks (play-round p1-deck p2-deck)))
	       (setq p1-deck (copy-list (first new-decks)))
	       (setq p2-deck (copy-list (second new-decks)))))
   (max (score p1-deck)
	(score p2-deck))))

(play-round (first inp) (second inp))

(defun config-seen (deck configs)
  (gethash deck configs))

(defun win (player xs ys)
  (let ((x (car xs))
	(rest-x (cdr xs))
	(y (car ys))
	(rest-y (cdr ys)))
    (if (eq player 'p1)
	(values 'p1
		(append rest-x (list x y))
		rest-y)
	(values 'p2
		rest-x
		(append rest-y (list y x))))))

(defun play-rec-round (xs ys configs)
  (let ((x (car xs))
	(rest-x (cdr xs))
	(y (car ys))
	(rest-y (cdr ys)))
    (if (config-seen xs configs)
	;; loop detected, player 1 wins
	(win 'p1 xs ys)
	
	(if (and (<= x (length rest-x))
		 (<= y (length rest-y)))
	    ;; recursive game
	    (win (play-rec-game (subseq rest-x 0 x)
				(subseq rest-y 0 y))
		 xs ys)
	    ;; standard game
	    (if (> x y)
		(win 'p1 xs ys)
		(win 'p2 xs ys))))))

(defun play-rec-game (p1-deck p2-deck)
  (let ((xs p1-deck)
	(ys p2-deck)
	(last-winner nil)
	(seen-configs (make-hash-table :test 'equal)))
    (loop :while (> (* (length xs)
		       (length ys))
		    0)
	  :do
	     ;; (format t "A: ~A~%B: ~A~%---~%" xs ys)
	     (multiple-value-bind (winner new-xs new-ys) (play-rec-round xs ys seen-configs)
	       (setf (gethash xs seen-configs) t)
	       (setq xs new-xs)
	       (setq ys new-ys)
	       (setq last-winner winner)))
    (values last-winner xs ys)))

(defun part2 (inp)
 (let ((p1-deck (copy-list (first inp)))
       (p2-deck (copy-list (second inp))))
   (multiple-value-bind (winner final-deck-1 final-deck-2) (play-rec-game p1-deck p2-deck)
     (declare (ignore winner))
     (max (score final-deck-1)
	  (score final-deck-2)))))

;; (require :sb-sprof)
;; (sb-sprof:with-profiling (:loop nil :report :flat)
;;   (part2 inp))


(format t "Part 2: ~A~%" (time (part2 inp)))
