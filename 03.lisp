(defparameter input-path #P"./inputs/03-input")

(require "asdf")
(asdf:load-system "uiop")

(defparameter inp (uiop:read-file-lines input-path))

(defun count-trees (inp xstep ystep)
 (let ((W (length (first inp))))
   (loop for step below (length inp)
      when (< (* ystep step) (length inp))
      count (let* ((x (* xstep step))
		   (x-wrap (mod x W))
		   (y (* ystep step))
		   (row (nth y inp))
		   (c (char row x-wrap)))
	      (eq c #\#)))))

(count-trees inp 3 1)

(reduce #'* (loop for xy in '((1 1)
		   (3 1)
		   (5 1)
		   (7 1)
		   (1 2))
    collect (count-trees inp
			 (first xy)
			 (second xy))))
