(defparameter input-path #P"./inputs/12-input")
(defparameter inp (load-input input-path))

(require "asdf")
(asdf:load-system "uiop")

(defun load-input (path)
  (mapcar (lambda (line)
	    (cons
	     (read-from-string (subseq line 0 1))
	     (parse-integer (subseq line 1))))
	  (uiop:read-file-lines path)))

(defparameter dirs
  '((e . (1 0))
    (s . (0 1))
    (w . (-1 0))
    (n . (0 -1))))

(defparameter cycle
  '((e . n)
    (n . w)
    (w . s)
    (s . e)))

(defun rotate (current-dir dir count)
  (let ((n-rot (mod (/ count 90) 4))
	(res current-dir))
    (when (eq dir 'r)
      (setq n-rot (- 4 n-rot)))
    (loop :repeat n-rot :do
	 (setq res (cdr (assoc res cycle))))
    res))

(defun move (current-pos dir count)
  (mapcar (lambda (x dx)
	    (+ x (* count dx)))
	  current-pos (cdr (assoc dir dirs))))

(rotate 'e 'r 90)

(apply #'+
       (loop :with dir = 'e
	  :with pos = '(0 0)
	  :for (inst . count) :in inp :do
	    (case inst
	      ((l r) (setq dir (rotate dir inst count)))
	      ((e s w n) (setq pos (move pos inst count)))
	      ((f) (setq pos (move pos dir count))))
	  :finally (return (values pos dir))))

(defun rotate-wp (wp dir count)
  (labels ((rot-ccw (pos)
	     (list (second pos)
		   (- 0 (first pos)))))
    (let ((n-rot (mod (/ count 90) 4))
	  (res (copy-list wp)))
      (when (eq dir 'r)
	(setq n-rot (- 4 n-rot)))
      (loop :repeat n-rot :do
	   (setq res (rot-ccw res)))
      res)))

(rotate-wp '(10 -1) 'r 90)

(defun move-wp (pos wp count)
  (mapcar (lambda (x dx)
	    (+ x (* count dx)))
	  pos wp))

(apply #'+
       (mapcar #'abs (loop :with wp = '(10 -1)
			:with pos = '(0 0)
			:for (inst . count) :in inp :do
			  (case inst
			    ((l r) (setq wp (rotate-wp wp inst count)))
			    ((e s w n) (setq wp (move wp inst count)))
			    ((f) (setq pos (move-wp pos wp count))))
			:finally (return (values pos wp)))))

