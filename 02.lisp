(defparameter input-path #P"./inputs/02-input")

(require "asdf")
(asdf:load-system "uiop")
(defun parse-line (line)
  (uiop:split-string line
		     :separator "- :"))
(defparameter in2 (mapcar #'parse-line (uiop:read-file-lines input-path)))

(defun pass-valid-p (pline)
  (let ((min (parse-integer (first pline)))
	(max (parse-integer (second pline)))
	(needle (char (third pline) 0))
	(pass (fifth pline)))
    (let ((count (loop for c across pass
		    count (eq c needle))))
      (and  (>= count min)
	    (<= count max)))))

(defun pass-valid-p-2 (pline)
  (let ((min (parse-integer (first pline)))
	(max (parse-integer (second pline)))
	(needle (char (third pline) 0))
	(pass (fifth pline)))
    (let ((first-ok (eq (char pass (- min 1))
			needle))
	  (second-ok (eq (char pass (- max 1))
			 needle)))
      (or
       (and first-ok (not second-ok))
       (and (not first-ok) second-ok)))))

(pass-valid-p (car (last in2)))

(loop for pline in in2
   count (pass-valid-p pline))

(loop for pline in in2
   count (pass-valid-p-2 pline))

