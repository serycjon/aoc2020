(defparameter input-path #P"./inputs/09-input")

(require "asdf")
(asdf:load-system "uiop")

(defparameter inp (mapcar #'parse-integer (uiop:read-file-lines input-path)))

(defun sums (preamble current)
  (loop :for x :in preamble
	:unless (= x current)
	  :collect (+ x current)))

(sums '(2 3 1 4 9) 3)

(defun update-queue (queue x)
  (cons x (butlast queue)))

(defun preamble-sums (inp)
  (maplist
   (lambda (xs)
     (sums (cdr xs) (car xs)))
   (reverse inp)))

(defun part1 (inp pre-size)
  (let* ((preamble (subseq inp 0 pre-size))
	 (preamble-sums (preamble-sums preamble)))
    (loop :for x :in (nthcdr pre-size inp)
	  :do
	     (if (not (some (lambda (sums)
			      (member x sums))
			    preamble-sums))
		 (return x)
		 (progn
		   (setq preamble
			 (update-queue preamble x))
		   (setq preamble-sums
			 (update-queue preamble-sums
				       (sums preamble x))))))))
(format t "Part 1: ~A~%" (part1 inp 25))

(defun part2 (inp pre-size)
  (let ((nums
	  (let ((wrong (part1 inp pre-size))
		(summands nil)
		(sum 0))
	    (loop :for x :in inp :do
	      (loop :while (> sum wrong) :do
		(decf sum (car (last summands)))
		(setq summands (butlast summands)))
	      (when (= sum wrong)
		(return summands))
	      (incf sum x)
	      (push x summands)))))
    (loop :for x :in nums
	  :maximize x :into max
	  :minimize x :into min
	  :finally (return (+ min max)))))

(format t "Part 2: ~A~%" (part2 inp 25))
