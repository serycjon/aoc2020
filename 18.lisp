(require "asdf")
(asdf:load-system "uiop")

(defun parse-line (line)
  (read-from-string  (format nil "(~A)" line)))

(defun make-ast (expr &optional aux)
  ;; (format t "~A  |  ~A~%" aux expr)
  (cond
    ((not expr) aux)
    ((atom expr) expr)
    ((not (cdr expr)) (make-ast (car expr) aux))
    ((not aux) (make-ast (cdr expr) (make-ast (car expr))))
    (t
     (let ((op (car expr))
	   (y (cadr expr)))
       (make-ast (cddr expr) (list op aux (make-ast y)))))))

(assert
 (= 13632
    (eval (make-ast (parse-line "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")))))

(format t "Part 1: ~A~%"
	(loop :for line :in (uiop:read-file-lines "inputs/18-input")
	      :sum (eval (make-ast (parse-line line)))))

(defun make-ast-2 (expr &optional aux)
  ;; (format t "~A  |  ~A~%" aux expr)
  (cond
    ((not expr) aux)
    ((atom expr) expr)
    ((not (cdr expr)) (make-ast-2 (car expr) aux))
    ((not aux) (make-ast-2 (cdr expr) (make-ast-2 (car expr))))
    (t
     (let ((op (car expr))
	   (y (cadr expr)))
       (if (eq op '*)
	   (list op aux (make-ast-2 (cdr expr)))
	   (make-ast-2 (cddr expr) (list op aux (make-ast-2 y))))))))

(assert
 (= 46
    (eval (make-ast-2 (parse-line "2 * 3 + (4 * 5)")))))

(assert
 (= 23340
    (eval (make-ast-2 (parse-line "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")))))

(format t "Part 2: ~A~%"
	(loop :for line :in (uiop:read-file-lines "inputs/18-input")
	      :sum (eval (make-ast-2 (parse-line line)))))
