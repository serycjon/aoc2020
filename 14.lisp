(defparameter input-path #P"./inputs/14-input")

(require "asdf")
(asdf:load-system "uiop")
(asdf:load-system "cl-ppcre")

(defun parse-mask-line (line)
  (subseq line 7))

(defun parse-mem-line (line)
  (cl-ppcre:register-groups-bind (addr value) ("mem\\[(\\d+)\\] = (\\d+)" line)
    (assert (and addr value))
    (cons (parse-integer addr) (parse-integer value))))

(defun parse-line (line)
  (if (string= "ma" (subseq line 0 2))
      (cons 'mask (parse-mask-line line))
      (cons 'mem (parse-mem-line line))))

(parse-line "mem[2144] = 7866321")
(parse-line "mask = 01")

(defun load-input (path)
  (loop :for line :in (uiop:read-file-lines path) :collecting (parse-line line)))

(defparameter inp (load-input input-path))

(defun to-binary (x)
  (format nil "~36,'0B" x))

(defun mask (x mask)
  (coerce (loop :for x-c :across x
		:for mask-c :across mask
		:collecting
		(if (char= mask-c #\X)
		    x-c
		    mask-c))
	  'string))

(mask "01011" "XXX0X")

(defun part1 (path)
 (let ((memory (make-hash-table))
       (current-mask nil)
       (instructions (load-input path)))
   (loop :for (inst-name . inst-values) :in instructions :do
     (case inst-name
       ((mask) (setq current-mask inst-values))
       ((mem) (let* ((binary-input (to-binary (cdr inst-values)))
		     (masked-input (mask binary-input current-mask)))
		(setf (gethash (car inst-values) memory)
		      masked-input)))))
   (loop :for bin-val :being :the :hash-values :in memory
	 :sum (parse-integer bin-val :radix 2))))

(format t "Part 1: ~A~%" (part1 input-path))

(defun replace-at-pos (x pos c)
  (with-output-to-string (stream)
    (format stream "~A" (subseq x 0 pos))
    (format stream "~A" c)
    (format stream "~A" (subseq x (1+ pos)))
    stream))

(let ((a "abcd"))
  (replace-at-pos a 2 #\X))

(defun unfloat (addr)
  (loop :for c :across addr
	:for i :below (length addr)
	:when (char= c #\X) :do
	  (return-from unfloat
	    (append (unfloat (replace-at-pos addr i #\0))
		    (unfloat (replace-at-pos addr i #\1)))))
  (list addr))

(unfloat "X1101X")

(defun mem-addr-decoder (addr mask)
  (let ((decoded (coerce (loop :for addr-c :across addr
			       :for mask-c :across mask
			       :collecting (if (char= mask-c #\0)
					       addr-c mask-c))
			 'string)))
    (unfloat decoded)))

(mem-addr-decoder "000000000000000000000000000000101010" "000000000000000000000000000000X1001X")

(defun part2 (path)
 (let ((memory (make-hash-table))
       (current-mask nil)
       (instructions (load-input path)))
   (loop :for (inst-name . inst-values) :in instructions :do
     (case inst-name
       ((mask) (setq current-mask inst-values))
       ((mem) (let* ((binary-addr (to-binary (car inst-values)))
		     (decoded-addrs (mem-addr-decoder binary-addr current-mask)))
		(loop :for addr :in decoded-addrs :do
		  (setf (gethash (parse-integer addr :radix 2) memory) (cdr inst-values)))))))
   (loop :for val :being :the :hash-values :in memory :sum val)))

(format t "Part 2: ~A" (part2 input-path))
