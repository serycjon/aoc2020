(defparameter input-path #P"./inputs/16-input")

(require "asdf")
(asdf:load-system "uiop")
(asdf:load-system "cl-ppcre")

(defun parse-rule (line)
  (destructuring-bind (name-string ranges-string) (cl-ppcre:split ": " line)
    (let ((name (read-from-string (first (cl-ppcre:split " " name-string))))
	  (ranges
	    (mapcar (lambda (range-str)
		      (mapcar #'parse-integer (cl-ppcre:split "-" range-str)))
		    (cl-ppcre:split " or " ranges-string))))
      (cons name ranges))))

(parse-rule "departure location: 37-479 or 485-954")

(defun parse-ticket (line)
  (mapcar #'parse-integer (cl-ppcre:split "," line)))

(parse-ticket "1,2,4")

(defun parse-input (path)
  (let* ((lines (uiop:read-file-lines path))
	 (rule-lines (subseq lines 0 20))
	 (my-ticket-line (car (subseq lines 22 23)))
	 (other-ticket-lines (subseq lines 25)))
    (list (mapcar #'parse-rule rule-lines)
	  (parse-ticket my-ticket-line)
	  (mapcar #'parse-ticket other-ticket-lines))))

(defparameter inp (parse-input input-path))
(defparameter example-inp
  (list (mapcar #'parse-rule '("class: 1-3 or 5-7"
			       "row: 6-11 or 33-44"
			       "seat: 13-40 or 45-50"))
	(parse-ticket "7,1,14")
	(mapcar #'parse-ticket '("7,3,47" "40,4,50" "55,2,20" "38,6,12"))))

(defun inside (x lb ub)
  (and (>= x lb) (<= x ub)))

(defun valid-p (value rule)
  (or (inside value (first (first rule)) (second (first rule)))
      (inside value (first (second rule)) (second (second rule)))))

(destructuring-bind (rules my-ticket other-tickets) inp
    (let ((error-rate 0))
      (loop :for ticket :in other-tickets :do
	(loop :for x :in ticket :do
	  (let ((is-valid nil))
	    (loop :for (rule-name . rule) :in rules
		  :when (valid-p x rule) :do
		    (setq is-valid t)
		    (return))
	    (unless is-valid
	      (incf error-rate x)))))
      error-rate))

(defparameter example2-inp
  (list (mapcar #'parse-rule '("class: 0-1 or 4-19"
			       "row: 0-5 or 8-19"
			       "seat: 0-13 or 16-19"))
	(parse-ticket "11,12,13")
	(mapcar #'parse-ticket '("3,9,18" "15,1,5" "5,14,9"))))

(destructuring-bind (rules my-ticket other-tickets) inp
  (let* ((tickets other-tickets)
	 (ticket-len (length my-ticket))
	 (valid-rules (loop :repeat ticket-len
			    :collect (loop :for i :below (length rules)
					   :collect i))))
    (loop :for ticket :in tickets :do
      (loop :for x :in ticket
	    :for i :below ticket-len :do
	      (let ((ticket-valid-rules
		      (loop :for (rule-name . rule) :in rules
			    :for rule-i :below (length rules)
			    :when (valid-p x rule) :collect rule-i)))
		(unless (= (length ticket-valid-rules) 0) ; ignore invalid tickets
		  (setf (nth i valid-rules) (intersection (nth i valid-rules)
							  ticket-valid-rules))))))

    ;; first pass complete, now we need to enforce single rule per ticket element
    (let ((exclusive))
      (loop :while (< (length exclusive) (length rules)) :do
	(destructuring-bind (rule-n ticket-pos)
	    (loop :for x :in valid-rules
		  :for i :below ticket-len
		  :when (and (= (length x) 1)
			     (not (member (first x) exclusive)))
		    :do
		       (setq exclusive (adjoin (first x) exclusive))
		       (return (list (first x) i)))
	  (loop :for i :below ticket-len
		:unless (= i ticket-pos) :do
		  (setf (nth i valid-rules) (set-difference (nth i valid-rules)
							    (list rule-n)))))))

    ;; collect the departure fields from our ticket
    (let ((dep-fields (loop :for i :below ticket-len
			    :when (eq 'departure (car
						  (nth
						   (car (nth i valid-rules))
						   rules)))
			      :collect (nth i my-ticket))))
      (apply #'* dep-fields))))
