(require "asdf")
(asdf:load-system "uiop")
(ql:quickload "cl-ppcre")

(defun parse-rule (line)
  (destructuring-bind (num-str rule-str)
      (cl-ppcre:split ": " line)
    (list (parse-integer num-str)
	  (mapcar
	   (lambda (r-side-str)
	     (mapcar
	      (lambda (x)
		(read-from-string x))
	      (cl-ppcre:split " " r-side-str)))
	   (cl-ppcre:split " \\| " rule-str)))))

(parse-rule "0: 4 1 5")
(parse-rule "1: 2 3 | 3 2")
(parse-rule "4: \"a\"")

(defun parse-input (path)
  (let ((lines (uiop:read-file-lines path))
	(rules (make-hash-table))
	(messages)
	(rule-p t))
    (loop :for line :in lines :do
	 (cond
	   ((= 0 (length line)) (setq rule-p nil))
	   (rule-p
	    (let ((rule (parse-rule line)))
	      (setf (gethash (first rule) rules)
		    (second rule))))
	   (t (push line messages))))
    (list rules (reverse messages))))

;; (defparameter inp (parse-input "inputs/19-simple"))
(defparameter inp (parse-input "inputs/19-input"))
;; (defparameter inp (parse-input "inputs/19-example-2"))

(defun seq-re (xs)
  ;; (format t "~a~%" xs)
  (apply #'concatenate 'string xs))

(defun or-re (xs)
  (format nil
	  "(~a)"
	  (subseq
	   (apply #'concatenate 'string
		  (mapcar (lambda (x)
			    (format nil "|(~a)" x))
			  xs))
	   1)))

(defun rule-regexp (rule rules &optional part2)
  (cond
    ((stringp rule) rule)
    ((and (numberp rule)
	  (= rule 8)
	  part2)
     (format nil "(~a)+"
	     (rule-regexp 42 rules part2)))
    ((and (numberp rule)
	  (= rule 11)
	  part2)
     (format nil "(~a){num}(~a){num}"
	     (rule-regexp 42 rules part2)
	     (rule-regexp 31 rules part2)))
    ((numberp rule) (rule-regexp (gethash rule rules) rules part2))
    ((= 1 (length rule))
     (seq-re (mapcar (lambda (x) (rule-regexp x rules part2))
		     (first rule))))
    (t (or-re
	(mapcar (lambda (xs)
		  (seq-re
		   (mapcar (lambda (x)
			     (rule-regexp x rules part2))
			   xs)))
		rule)))))

(format t "part 1: ~a~%"
	(let ((re (cl-ppcre:create-scanner
		   (format nil "^~a$"
			   (rule-regexp 0 (first inp))))))
	  (loop :for line :in (second inp)
		:count (cl-ppcre:scan re line))))

(defun grammar-regexp (template num)
  (let ((re (cl-ppcre:regex-replace-all "num"
					template
					(format nil "~a" num))))
    (cl-ppcre:create-scanner re)))

(defun p2match (line scanners)
  (some (lambda (scanner)
	  (cl-ppcre:scan scanner line))
	scanners))

(format t "part 2: ~a~%"
 (let* ((re-template
	  (format nil "^~a$" (rule-regexp 0 (first inp) t)))
	(scanners (loop :for i :from 1 :to 96
			:collect (grammar-regexp re-template i))))
   (loop :for line :in (second inp)
	 :count
	 (p2match line scanners))))
