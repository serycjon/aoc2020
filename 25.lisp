(defun try-loop-size (target &key (init 7) (mod 20201227))
  (loop :for value = 1 :then (mod (* value init)
				  mod)
     :for i = 0 :then (1+ i)
     :when (= target value) :do
       (return i)))

(defun transform (init loop &optional (mod 20201227))
  (loop :repeat loop
     :for value = init :then (mod (* value init)
			       mod)
     :finally (return value)))

(transform 7 8)
(transform 7 11)
(try-loop-size 5764801)
(try-loop-size 17807724)

(format t "Part 1: ~a~%"
	(let ((card-loop (try-loop-size 8335663))
	      (door-loop (try-loop-size 8614349)))
	  (transform 8335663 door-loop)))

6408263

