(defparameter input-path #P"./inputs/13-input")

(require "asdf")
(asdf:load-system "uiop")

(defun load-input (path)
  (destructuring-bind (a b) (uiop:read-file-lines path)
    (cons (parse-integer a)
	  (mapcar (lambda (x)
		    (unless (string= x "x")
		      (parse-integer x)))
		  (uiop:split-string b :separator ",")))))
(defparameter inp (load-input input-path))

(format t "Part 1: ~A"
	(let ((best-bus)
	      (best-time (first inp))
	      (dep-time (first inp)))
	  (loop :for bus :in (cdr inp)
		:when bus :do
		  (let ((next-in (- bus (mod dep-time bus))))
		    (when (< next-in best-time)
		      (setq best-time next-in)
		      (setq best-bus bus))))
	  (* best-time best-bus)))

(defun parse-inp (inp)
  (loop :for i :below (length (cdr inp))
     :for x :in (cdr inp)
     :when x :collect (cons x i)))

(defparameter inp2 (parse-inp inp))

(defun extended-euclidean (a b)
  "from wikipedia pseudo-code.
returns integers '(gcd bezout-a bezout-b) such that:
bezout-a * a + bezout-b * b = gcd"
  (let ((old-r a) (r b)
	(old-s 1) (s 0)
	(old-te 0) (te 1))
    (loop :while (not (= r 0)) :do
      (let* ((quotient (floor old-r r))
	     (new-r (- old-r (* quotient r)))
	     (new-s (- old-s (* quotient s)))
	     (new-te (- old-te (* quotient te))))
	(setq old-r r) (setq r new-r)
	(setq old-s s) (setq s new-s)
	(setq old-te te) (setq te new-te)))
    (let ((gcd old-r)
	  (bezout-a old-s)
	  (bezout-b old-te))
      (list gcd bezout-a bezout-b))))

(extended-euclidean 240 46)

(defun chinese-remainder-pair (bus-a bus-b)
  "Merge two buses (cons bus-interval bus-offset) into one virtual bus"
  (destructuring-bind (gcd bez-a bez-b) (extended-euclidean (car bus-a) (car bus-b))
    (assert (= gcd 1))
    (let ((solution (+ (* (cdr bus-b) bez-a (car bus-a))
		       (* (cdr bus-a) bez-b (car bus-b))))
	  (new-car (* (car bus-a) (car bus-b))))
      (cons new-car
	    (mod solution new-car) 	; modulo to keep things small
	    ))))

(chinese-remainder-pair '(3 . 0) '(4 . 3))

(mod -9 12)
(reduce #'chinese-remainder-pair (parse-inp '(blab 17 () 13 19)))

(format t "Part 2: ~A" (let ((last-pair (reduce #'chinese-remainder-pair inp2)))
			 (- (car last-pair) (cdr last-pair)) ; undo the virtual single bus offset
			 ))
