(defparameter input-path #P"./inputs/01-input")

(require "asdf")
(asdf:load-system "uiop")
(defparameter in1 (mapcar #'parse-integer (uiop:read-file-lines input-path)))

(loop named top
   for i from 0 below (length in1)
   for a in in1
   do (loop for j from 0 below (length in1)
	 for b in in1
	 when (and (not (= i j))
		   (= (+ a b) 2020))
	 do (return-from top (* a b))))

(loop named top
   for i from 0 below (length in1)
   for a in in1
   do (loop for j from 0 below (length in1)
	 for b in in1 do
	   (loop for k below (length in1)
	      for c in in1
	      when (and (not (= i j))
			(not (= i k))
			(= (+ a b c) 2020))
	      do (return-from top (* a b c)))))
