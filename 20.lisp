(require "asdf")
(asdf:load-system "uiop")

(uiop:split-string "Tile 3079:" :separator " :")

(defun parse-tile (lines)
  (let ((tile-n (parse-integer (subseq (first lines) 5) :junk-allowed t)))
    (list tile-n (make-array (list (length (cdr lines))
				   (length (cdr lines)))
			     :initial-contents (cdr lines)))))

(defun parse-input (path)
  (let ((lines (uiop:read-file-lines path))
	(split-lines nil)
	(current-tile-line nil))
    (loop :for line :in lines :do
      (cond
	((= 0 (length line))
	 (push (reverse current-tile-line) split-lines)
	 (setq current-tile-line nil))
	(t
	 (push line current-tile-line))))
    (push (reverse current-tile-line) split-lines)
    (mapcar #'parse-tile split-lines)))

(defun extract-border (tile)
  (let* ((sz (array-dimension tile 0))
	 (N) (S) (E) (W))
    (loop :for row :below sz :do
      (loop :for col :below sz
	    :for elem = (aref tile row col)
	    :do
	       (when (= row 0) (push elem N))
	       (when (= col 0) (push elem W))
	       (when (= row (- sz 1)) (push elem S))
	       (when (= col (- sz 1)) (push elem E))))
    (mapcar (lambda (xs)
	      (coerce (reverse xs) 'string))
	    (list N E S W))))

(defparameter inp (parse-input "inputs/20-example"))
(defparameter inp (parse-input "inputs/20-input"))

(defun rotate (tile)
  (let* ((N (array-dimension tile 0))
	 (res (make-array `(,N ,N))))
    (loop :for row :below N :do
      (loop :for col :below N :do
	(setf (aref res row col)
	      (aref tile (- N col 1) row))))
    res))

(defun mirror (tile)
 (let* ((N (array-dimension tile 0))
	 (res (make-array `(,N ,N))))
    (loop :for row :below N :do
      (loop :for col :below N :do
	(setf (aref res row col)
	      (aref tile row (- N col 1)))))
    res))

(defun tile-repr (tile)
  (with-output-to-string (stream)
    (format stream "~%")
    (destructuring-bind (H W) (array-dimensions tile)
      (dotimes (r H)
	(dotimes (c W)
	  (format stream "~a" (aref tile r c)))
	(format stream "~%")))))

(defparameter configs '(orig r rr rrr m mr mrr mrrr))

(defparameter config-functions
  (list (cons 'orig #'identity)
	(cons 'r #'rotate)
	(cons 'rr (lambda (tile) (rotate (rotate tile))))
	(cons 'rrr (lambda (tile) (rotate (rotate (rotate tile)))))
	(cons 'm #'mirror)
	(cons 'mr (lambda (tile) (rotate (mirror tile))))
	(cons 'mrr (lambda (tile) (rotate (rotate (mirror tile)))))
	(cons 'mrrr (lambda (tile) (rotate (rotate (rotate (mirror tile))))))))

(tile-repr (rotate (cadar inp)))

(defun get-all-config-borders (tile)
 (loop :for conf :in configs
       :collect
       (cons conf
	     (extract-border
	      (funcall (cdr (assoc conf config-functions))
		       tile)))))

(defun match (my-borders other-borders side)
  "borders: list of (N E S W) borders
side: index into my-borders"
  (cons my-borders other-borders)
  (let ((my-border (nth side my-borders))
	(other-border (nth (mod (+ side 2) 4) other-borders)))
    (string= my-border other-border)))

(get-all-config-borders (cadar inp))

(match (extract-border (cadar inp))
  (extract-border (mirror (cadar inp)))
  3)

(defun compatible-configs (tile-n north-tile west-tile tile-config-map)
  "return list of all configs of tile-n compatible with the north and west neighbors
north-tile and west-tile are (N E S W) borders or nil"
  (let ((tile-configs (gethash tile-n tile-config-map)))
    (loop :for config :in configs
	  :for borders = (cdr (assoc config tile-configs))
	  :when (and (or (null north-tile)
			 (match borders north-tile 0))
		     (or (null west-tile)
			 (match borders west-tile 3)))
	    :collect config)))

(defun row-major2rc (i array)
  (let ((W (array-dimension array 1)))
    (multiple-value-bind (r c) (floor i W)
      (list r c))))

(defun place-tile (grid lin-pos tile-config-map available-tiles)
  (list grid lin-pos (loop :for k :being :the hash-key :of available-tiles :collect k))
  ;; grid stores pairs: (tile-num . tile-config)
  (when (= lin-pos (array-total-size grid))
    (return-from place-tile grid))
  
  (destructuring-bind (r c) (row-major2rc lin-pos grid)
    (let ((north-neighb (and (array-in-bounds-p grid (- r 1) c)
			     (let ((neigh (aref grid (- r 1) c)))
			       (cdr (assoc (cdr neigh) (gethash (car neigh) tile-config-map))))))
	  (west-neighb (and (array-in-bounds-p grid r (- c 1))
			    (let ((neigh (aref grid r (- c 1))))
			      (cdr (assoc (cdr neigh) (gethash (car neigh) tile-config-map)))))))
      (loop :for tile-n being the hash-key of available-tiles :do
	(let ((compat (compatible-configs tile-n north-neighb west-neighb tile-config-map)))
	  (cons tile-n compat)
	  (when compat
	    (loop :for config :in compat :do
	      (let ((new-grid (copy-grid grid))
		    (new-avail (make-hash-table)))
		(setf (aref new-grid r c) (cons tile-n config))
		(loop :for k :being :the hash-key :of available-tiles
		      :when (not (= k tile-n)) :do
			(setf (gethash k new-avail) t))
		(let ((result (place-tile new-grid (1+ lin-pos) tile-config-map new-avail)))
		  (when result (return-from place-tile result))))))))))
  nil)

(defun get-tile-config-map (inp)
 (let ((tile-config-map (make-hash-table)))
   (loop :for (tile-num tile) :in inp :do
     (setf (gethash tile-num tile-config-map)
	   (get-all-config-borders tile)))
   tile-config-map))

(defun get-tiles-map (inp)
 (let ((tiles-map (make-hash-table)))
   (loop :for (tile-num tile) :in inp :do
     (setf (gethash tile-num tiles-map)
	   tile))
   tiles-map))

(defun copy-grid (grid)
  (let* ((dimensions (array-dimensions grid))
	 (new-grid (make-array dimensions)))
    (dotimes (i (array-total-size grid))
      (setf (row-major-aref new-grid i)
	    (cons (car (row-major-aref grid i))
		  (cdr (row-major-aref grid i)))))
    new-grid))

(defvar t-conf-map nil)
(defvar grd nil)
(defvar t-map nil)

(defun part1 (inp)
  (let* ((N (round (sqrt (length inp))))
	 (grid (make-array (list N N) :initial-element nil))
	 (tile-config-map (get-tile-config-map inp))
	 (tiles-map (get-tiles-map inp))
	 (available-tiles (make-hash-table)))
    (loop :for (id . _) :in inp :do (setf (gethash id available-tiles) t))
    (setq grid (place-tile grid 0 tile-config-map available-tiles))
    (setq t-conf-map tile-config-map)
    (setq grd grid)
    (setq t-map tiles-map)
    (* (car (aref grid 0 0))
       (car (aref grid 0 (1- N)))
       (car (aref grid (1- N) 0))
       (car (aref grid (1- N) (1- N))))))

(defun reconstruct-image (p1-grid tile-map tile-sz)
  (let* ((image (make-array (list (* tile-sz (array-dimension p1-grid 0))
				  (* tile-sz (array-dimension p1-grid 0))))))
    (loop :for row :below (array-dimension p1-grid 0) :do
      (loop :for col :below (array-dimension p1-grid 1) :do
	(let ((tile (destructuring-bind (tile-num . tile-config) (aref p1-grid row col)
		      (funcall (cdr (assoc tile-config config-functions)) (gethash tile-num tile-map)))))
	  (loop :for t-row :below tile-sz :do
	    (loop :for t-col :below tile-sz :do
	      (setf (aref image
			  (+ (* tile-sz row)
			     t-row)
			  (+ (* tile-sz col)
			     t-col))
		    (aref tile (1+ t-row) (1+ t-col))))))))
    image))

(defparameter monster-pattern
  '("                  # "
    "#    ##    ##    ###"
    " #  #  #  #  #  #   "))

(defparameter monster-coords
  (let ((coords))
    (loop :for line :in monster-pattern
	  :for row :below (length monster-pattern) :do
	    (loop :for c :across line
		  :for col :below (length line)
		  :when (char= c #\#) :do
		    (push (list row col) coords)))
    coords))

(defun monster-present (image row col)
  (loop :for (d-row d-col) :in monster-coords
	:when (or (not (array-in-bounds-p image (+ row d-row) (+ col d-col)))
		  (not (char= (aref image (+ row d-row) (+ col d-col)) #\#)))
	  :do (return-from monster-present nil))
  t)

(defun mark-monster (counter row col)
  (loop :for (d-row d-col) :in monster-coords :do
    (setf (aref counter (+ row d-row) (+ col d-col)) 1))
  counter)

(defun mark-monsters (image)
  (let ((counter (make-array (array-dimensions image)))
	(sz (array-dimension image 0)))
    (loop :for row :below sz :do
      (loop :for col :below sz
	    :when (monster-present image row col)
	      :do (mark-monster counter row col)))
    counter))

(defun count-monsters (counter)
  (loop :for row :below (array-dimension counter 0) :sum
    (loop :for col :below (array-dimension counter 0)
	  :count (> (aref counter row col) 0))))

(defun part2 (inp)
  (part1 inp)
  (let* ((p1-grid grd)
	 (tile-map t-map)
	 (tile-sz (- (array-dimension (second (first inp)) 0) 2))
	 (image (reconstruct-image p1-grid tile-map tile-sz))
	 (n-px (loop :for i :below (array-total-size image)
		     :count (char= #\# (row-major-aref image i)))))
    (loop :for (blab . transformation) :in config-functions :do
      (let ((monster-px-count (count-monsters
			       (mark-monsters
				(funcall transformation image)))))
	(when (> monster-px-count 0)
	  (return-from part2 (- n-px monster-px-count)))))
    ))

(format t "Part 1: ~a~%" (part1 inp))
(format t "Part 2: ~a~%" (part2 inp))
