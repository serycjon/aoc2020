(require "asdf")
(asdf:load-system "uiop")
(ql:quickload "cl-ppcre")

(defun parse-input (path)
  (loop :for line :in (uiop:read-file-lines path)
     :collect
       (destructuring-bind (food-str ing-str)
	   (cl-ppcre:split " \\(contains " line)
	 (let ((ingredients
		(read-from-string
		 (format nil "(~a)"
			 food-str)))
	       (allergens
		(read-from-string
		 (format nil "(~a"
			 (cl-ppcre:regex-replace-all
			  ","
			  ing-str
			  "")))))
	   (list ingredients
		 allergens)))))

(defparameter inp (parse-input "inputs/21-example"))
(defparameter inp (parse-input "inputs/21-input"))

(let ((all-map (make-hash-table)))
  (loop :for (ingredients allergens) :in inp :do
       (loop :for all :in allergens :do
	    (setf (gethash all all-map)
		  (if (gethash all all-map)
		      (intersection
		       ingredients
		       (gethash all all-map))
		      ingredients))))
  (loop :for all :being :the hash-key :of all-map
     :collect (cons all
		    (gethash all all-map))))
((SOY SQJHC FVJKL)
 (FISH SQJHC MXMXVKD)
 (DAIRY MXMXVKD))

((EGGS SNHRPV ZMB)
 (PEANUTS VFLMS BMRMHM)
 (SOY RKKRX BMRMHM QZKJRTL)
 (FISH BMRMHM RKKRX)
 (WHEAT RKKRX)
 (SHELLFISH
  BQTVR
  BQKNDVB
  VFLMS
  QZKJRTL)
 (DAIRY VFLMS BQKNDVB)
 (NUTS BMRMHM BQKNDVB SNHRPV))

;; manually uniquify
(let ((all ;; '((soy . fvjkl)
	   ;;   (fish . sqjhc)
	   ;;   (dairy . mxmxvkd))
       '((EGGS . ZMB)
	 (PEANUTS . VFLMS)
	 (SOY . QZKJRTL)
	 (FISH . BMRMHM)
	 (WHEAT . RKKRX)
	 (SHELLFISH . BQTVR)
	 (DAIRY . BQKNDVB)
	 (NUTS . SNHRPV))))
  (loop for (ingreds alls) in inp :sum
       (loop :for ing :in ingreds
	  :count (not (rassoc ing all)))))

;; manually sort and output
'((DAIRY . BQKNDVB)
  (EGGS . ZMB)
  (FISH . BMRMHM)
  (NUTS . SNHRPV)
  (PEANUTS . VFLMS)
  (SHELLFISH . BQTVR)
  (SOY . QZKJRTL)
  (WHEAT . RKKRX))

bqkndvb,zmb,bmrmhm,snhrpv,vflms,bqtvr,qzkjrtl,rkkrx
