(defparameter input-path #P"./inputs/10-input")

(require "asdf")
(asdf:load-system "uiop")

(defparameter inp (mapcar #'parse-integer (uiop:read-file-lines input-path)))

(format t "part 1: ~A~%"
 (let* ((inp (copy-list inp))
	(sorted (cons 0 (sort inp #'<)))
	(diffs (mapcar #'- (cdr sorted) sorted)))
   (loop :for diff :in diffs
	 :count (= 1 diff) :into ones
	 :count (= 3 diff) :into threes
	 :finally (return (* ones (+ 1 threes))))))

(defparameter test-inp '(16 10 15 5 1 11 7 19 6 12 4))

(format t "part 2: ~A~%"
 (let* ((inp (cons (+ 3 (apply #'max inp)) (copy-list inp)))
	(sorted (sort inp #'<))
	;; number of combinations for every target joltage
	;; starts with -2 to skip bounds checks
	(counts (make-array (+ 3 (car (last sorted))) :initial-element 0)))
   (setf (aref counts (+ 2 0)) 1) 	; joltage 0 achieved in 1 possible way
   (loop :for x :in sorted :do
     (setf (aref counts (+ 2 x))
	   (+ (aref counts (+ 2 (- x 1)))
	      (aref counts (+ 2 (- x 2)))
	      (aref counts (+ 2 (- x 3))))))
   (values (aref counts (- (length counts) 1))
	   counts)))
