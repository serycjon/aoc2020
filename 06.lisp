(defparameter input-path #P"./inputs/06-input")

(require "asdf")
(asdf:load-system "uiop")

(defun parse-input (lines)
  (let ((result)
	(current))
    (dolist (line lines)
      (if (= 0 (length line))
	  (progn (push current result)
		 (setq current nil))
	  (push
	   (loop for c across line collect c)
	   current)))
    (push current result)
    result))

(defparameter inp (parse-input (uiop:read-file-lines input-path)))

(format t "First task: ~A~%" 
	(loop for entry in inp
	   sum (length (reduce #'union entry
			       :initial-value
			       (first entry)))))

(format t "Second task: ~A~%" 
	(loop for entry in inp
	   sum (length (reduce #'intersection
			       entry
			       :initial-value
			       (first entry)))))
