(require "asdf")
(asdf:load-system "uiop")

(defun parse-input (line)
  (mapcar
   #'parse-integer
   (uiop:split-string line :separator ",")))

(defparameter inp (parse-input "0,3,6"))
(defparameter inp (parse-input "6,3,15,13,1,0"))

(defun store (x addr mem)
  (if (not (gethash addr mem))
      (setf (gethash addr mem) (cons x x))
      (progn
	(setf (cdr (gethash addr mem)) (car (gethash addr mem)))
	(setf (car (gethash addr mem)) x))))

(defun num-age (mem-entry)
  (- (car mem-entry) (cdr mem-entry)))

(defun solve (n-iter)
 (let ((mem (make-hash-table))
       (time 0)
       (last-num))
   (loop for n in inp :do
     (store (incf time) n mem)
     (setf last-num n))
   (loop while (< time n-iter) :do
     (let ((n (num-age (gethash last-num mem))))
       (store (incf time) n mem)
       (setf last-num n)))
   last-num))

(format t "Part 1: ~A~%" (solve 2020))
(format t "Part 2: ~A~%" (solve 30000000))
