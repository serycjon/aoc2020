(require "asdf")

(defun parse-input (cups-str)
  (loop :for c :across cups-str
     :collect (parse-integer
	       (format nil "~a" c))))

(defparameter example-inp (parse-input "389125467"))
(defparameter inp (parse-input "398254716"))

(setq *print-circle* t)

(defun destination (cups lb ub)
  (let ((target (1- (car cups)))
	(picked (list (second cups)
		      (third cups)
		      (fourth cups))))
    (when (< target lb)
	   (setf target ub))
    (loop :while (member target picked) :do
	 (decf target)
	 (when (< target lb)
	   (setf target ub)))
    target))

(defun swap (current-parent new-parent)
  (let ((three (cdr current-parent))
	(last-from-three (cdddr current-parent))
	(after-three (cddddr current-parent))
	(new-after-three (cdr new-parent)))
    (setf (cdr current-parent) after-three)
    (setf (cdr new-parent) three)
    (setf (cdr last-from-three) new-after-three))
  current-parent)

(defun move (cups lb ub cup-cache)
  (let* ((target-num (destination cups lb ub))
	 (target (gethash target-num cup-cache)))
    ;; (loop :while (not (= (car target) target-num)) :do
    ;;   (setq target (cdr target)))
    ;; (format t "source: ~a~%" cups)
    ;; (format t "destination: ~a ~a~%" target-num target)
    (swap cups target)
    ;; (format t "swapped: ~a~%" cups)
    )
  ;; (format t "---~%")
  )

(defun part1 (inp)
 (let* ((cups (copy-list inp))
	(lb (apply #'min cups))
	(ub (apply #'max cups))
	(cup-cache (make-hash-table)))
   ;; make cup cache/index
   (loop :for x = cups :then (cdr x) :do
     (if (null x)
	 (return)
	 (setf (gethash (car x) cup-cache) x)))

   ;; make circluar list
   (setf (cdr (last cups)) cups)
  
   ;; play
   (loop :repeat 100 :do
     (move cups lb ub cup-cache)
     (setf cups (cdr cups)))

   (loop :while (not (= 1 (car cups))) :do
     (setf cups (cdr cups)))

   (setf cups (cdr cups))
   (with-output-to-string (stream)
     (loop :while (not (= 1 (car cups))) :do
       (format stream "~a" (car cups))
       (setf cups (cdr cups))))))

(format t "Part 1: ~a~%" (part1 inp))

(defun part2-extend-inp (inp &optional (N 1000000))
  (let ((xs (reverse inp)))
    (loop :for x :from (1+ (length xs)) :to N :do
      (push x xs))
    (reverse xs)))

(defun part2 (inp)
  (let* ((cups (part2-extend-inp inp))
	 (cup-cache (make-hash-table)))
    ;; make cup cache/index
    (loop :for x = cups :then (cdr x) :do
      (if (null x)
	  (return)
	  (setf (gethash (car x) cup-cache) x)))
    
    ;; make circluar list
    (setf (cdr (last cups)) cups)
  
    ;; play
    (loop :repeat 10000000 :do
      (move cups 1 1000000 cup-cache)
      (setf cups (cdr cups)))

    (loop :while (not (= 1 (car cups))) :do
      (setf cups (cdr cups)))

    (setf cups (cdr cups))
    (values (* (car cups)
	       (cadr cups))
	    (car cups)
	    (cadr cups))))

(format t "Part 2: ~a~%" (part2 inp))
