(declaim (optimize (speed 3) (safety 0)))
(require "asdf")
(asdf:load-system "uiop")

(defun parse-input (path)
  (let ((state (make-hash-table :test 'equal))
	(lines (uiop:read-file-lines path)))
    (loop :for line :in lines
	  :for row :below (length lines) :do
	    (loop :for c :across line
		  :for col :below (length line)
		  :when (char= c #\#) :do
		    (setf (gethash (list col row 0 0) state) t)))
    state))

;; (defparameter inp (parse-input "inputs/17-example"))
(defparameter inp (parse-input "inputs/17-input"))

(defparameter deltas-3d
  (let ((res))
    (loop :for dx fixnum :in '(-1 0 1) :do
      (loop :for dy fixnum :in '(-1 0 1) :do
	(loop :for dz fixnum :in '(-1 0 1)
	      :unless (= dx dy dz 0) :do
		(push (list dx dy dz 0) res))))
    res))

(defun neighbor-positions-3d (pos)
  (loop :for delta :in deltas-3d
	:collect
	(mapcar (lambda (a b)
		  (declare (type fixnum a b))
		  (the fixnum (+ a b)))
		pos delta)))

(defparameter deltas-4d
 (let ((res))
    (loop :for dx fixnum :in '(-1 0 1) :do
      (loop :for dy fixnum :in '(-1 0 1) :do
	(loop :for dz fixnum :in '(-1 0 1) :do
	  (loop :for dw fixnum :in '(-1 0 1)
		:unless (= dx dy dz dw 0) :do
		  (push (list dx dy dz dw) res)))))
   res))

(defun neighbor-positions-4d (pos)
  (loop :for delta :in deltas-4d
	:collect
	(mapcar (lambda (a b)
		  (declare (type fixnum a b))
		  (the fixnum (+ a b)))
		pos delta)))

(defun neighbor-count (state pos neighb-fn)
  (declare (type function neighb-fn))
  (loop :for new-pos :in (funcall neighb-fn pos)
	:count (gethash new-pos state)))

(defun interesting-positions (state neighb-fn)
  (declare (type function neighb-fn))
  (let ((result (make-hash-table :test 'equal)))
    (loop :for k being the hash-key
	    using (hash-value v) of state
	  :when v :do
	    (setf (gethash k result) t)
	    (loop :for pos :in (funcall neighb-fn k) :do
	      (setf (gethash pos result) t)))
    (loop :for pos being the hash-key of result
	  :collect pos)))

(defun print-slice (state z)
  (destructuring-bind (min-x max-x min-y max-y) 
      (loop :for k being the hash-key
	      using (hash-value v) of state
	    :when (and (= z (third k))
		       v)
	      :minimize (first k) :into min-x
	    :maximize (first k) :into max-x
	    :minimize (second k) :into min-y
	    :maximize (second k) :into max-y
	    :finally (return (list min-x max-x min-y max-y)))

    (loop :for y :from min-y :to max-y :do
      (loop :for x :from min-x :to max-x :do
	(format t "~A" (if (gethash (list x y z 0) state)
			   #\# #\.)))
      (format t "~%"))))

;; (interesting-positions inp #'neighbor-positions-3d)

(defun update-state (state neighb-fn)
 (let ((new-state (make-hash-table :test 'equal)))
   (loop :for pos :in (interesting-positions state neighb-fn) :do
     (let ((count (the fixnum (neighbor-count state pos neighb-fn))))
       (when (or (and (gethash pos state)
		      (or (= 2 count) (= 3 count)))
		 (and (not (gethash pos state))
		      (= 3 count)))
	 (setf (gethash pos new-state) t))))
   new-state))

(defun iter-n-times (state n neighb-fn)
  (declare (type fixnum n))
  (let ((current state))
    (loop :repeat n :do
      (setq current (update-state current neighb-fn)))
    current))

(format t "Part 1: ~A~%"
	(time (loop :for v being the hash-value of (iter-n-times inp 6 #'neighbor-positions-3d)
		    :count v)))

(format t "Part 2: ~A~%"
	(time (loop :for v being the hash-value of (iter-n-times inp 6 #'neighbor-positions-4d)
		    :count v)))

(require :sb-sprof)
(sb-sprof:with-profiling (:loop nil :report :flat)
  (loop :for v being the hash-value of (iter-n-times inp 6 #'neighbor-positions-4d)
	:count v))
